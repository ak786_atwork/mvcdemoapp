package com.example.mvcdemoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private String TAG = "MVCLog";
    private StudentController controller;
    public TextView tvName,tvRoll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Student model = retrieveStudentFromDatabase();
        controller = new StudentController(model,new MainActivity());
        controller.updateView();
    }
    private Student retrieveStudentFromDatabase() {
        Student student = new Student();
        student.setName("Robert");
        student.setRollNo("10");
        return student;
    }
    public void printStudentDetails(String studentName, String studentRollNo){
        Log.d(TAG, "printStudentDetails: "+"Name: " + studentName);
        Log.d(TAG, "printStudentDetails: "+"Roll No: " + studentRollNo);
    }
    public void btnUpdateUI(View view) {
        // update model data
        controller.setStudentName("Rusho");
        controller.setStudentRoll("12");
        controller.updateView();
    }
}